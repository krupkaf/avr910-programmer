#include <Arduino.h>
#include <HardwareSerial.h>
#include <SPI.h>
#include <avr/pgmspace.h>

#ifdef UNO_AVR_ISP_SHIELD
#define BEEPPin 6
#define LEDPin 7
#define ELEDPin 8
#define HLEDPin 9
#define RESETNpin 10

#define SPI_INIT  pinMode(11, OUTPUT);pinMode(13, OUTPUT);pinMode(12, INPUT);
#endif

//LED / PROGRAM LED
#define LEDON digitalWrite(LEDPin, HIGH)
#define LEDOFF digitalWrite(LEDPin, LOW)
#define LEDINIT pinMode(LEDPin, OUTPUT)

//ERROR LED
#define ELEDON digitalWrite(ELEDPin, HIGH)
#define ELEDOFF digitalWrite(ELEDPin, LOW)
#define ELEDINIT pinMode(ELEDPin, OUTPUT)

//HEART LED
#define HLEDON digitalWrite(HLEDPin, HIGH)
#define HLEDOFF digitalWrite(HLEDPin, LOW)
#define HLEDINIT pinMode(HLEDPin, OUTPUT)

//BEEP
#define BEEPON digitalWrite(BEEPPin, HIGH)
#define BEEPOFF digitalWrite(BEEPPin, LOW)
#define BEEPINIT pinMode(BEEPPin, OUTPUT)

//Reset
#define RESETN_INIT pinMode(RESETNpin, OUTPUT)
#define RESETN_DISABLE pinMode(RESETNpin, INPUT)
#define RESETN_SET digitalWrite(RESETNpin, LOW)
#define RESETN_CLR digitalWrite(RESETNpin, HIGH)

/** byte write */
typedef struct {
  /**avr910-devcode*/
  unsigned char devcode;
  /**Code during polling Flash. 0x00: no polling supported, program default
   * time*/
  unsigned char polling;
} TDev_S;

/** Devices which support Page Programming.  */
typedef struct {
  /**avr910-devcode*/
  unsigned char devcode;
  /**Pagesite in Words
   * Pagesize 32 words ~ 0x20
   */
  unsigned char flashPageSize;
  unsigned char eepromPageSize;
} TDev_M;

typedef struct {
  /**Kod aktualne nastaveneho zarizeni - nastaveni provedeno pres prikaz T.*/
  unsigned char devcode;
  /**Pagesite in Words*/
  unsigned char flashPageSize;
  unsigned char eepromPageSize;
  /**Code during polling Flash.*/
  unsigned char polling;
  /**Address*/
  unsigned short addr;
} TSetting;
TSetting setting;

#define SW_ID "AVR ISP"
#define CHIP_ID "Build: " __TIME__ " " __DATE__
#define SW_VER "10"
#define HW_VER "10"

/** byte write */
const TDev_S Dev_S[] PROGMEM = {
    {0x13, 0x00}, // AT90S1200
    {0x20, 0x7F}, // AT90S2313
    {0x28, 0x7f}, // AT90s4414
    {0x30, 0xff}, // AT90s4433
    {0x34, 0xFF}, // AT90S2333
    {0x38, 0x7F}, // AT90S8515
    {0x4c, 0xFF}, // AT90s2343 (also AT90s2323 and ATtiny22)
    {0x55, 0xFF}, // tn12
    {0x56, 0xFF}, // tn15
    {0x68, 0xFF}, // AT90S8535
    {0x6c, 0xff}, // AT90s4434
    {0x00, 0x00}};

/** Devices which support Page Programming. */
const TDev_M Dev_M[] PROGMEM = {
    {0x20, 0x10,
     0x04}, // ATtiny25, ATtiny45, ATtiny85, ATtiny24, ATtiny44, ATtiny84
    {0x31, 0x20, 0x04}, // ATmega48
    {0x33, 0x20, 0x04}, // ATmega88
    {0x35, 0x40, 0x04}, // ATmega168
    {0x3A, 0x20, 0x00}, // ATmega8515
    {0x41, 0x40, 0x00}, // ATmega103
    {0x43, 0x40,
     0x00}, // ATmega128 ,AT90CAN128 ?, AT90CAN64 ?, AT90CAN32 ?, ATmega640 ?,
            // ATmega1280 ?, ATmega1281 ?, ATmega2560 ?, ATmega2561 ?,
            // ATmega128RFA1 ?, ATmega32u4 ?, AT90USB646 ?, AT90USB647 ?,
            // AT90USB1286 ?, AT90USB1287 ?
    {0x45, 0x40, 0x00}, // ATmega64
    {0x5e, 0x10, 0x00}, // ATtiny26, ATtiny261 ?, ATtiny461 ?, ATtiny861 ?,
                        // ATtiny2313, ATtiny4313,
    {0x60, 0x40, 0x00}, // ATmega161
    {0x63, 0x40, 0x00}, // ATmega162
    {0x64, 0x40, 0x00}, // ATmega163
    {0x69, 0x20, 0x00}, // ATmega8535
    {0x72, 0x40, 0x00}, // ATmega32
    {0x74, 0x40, 0x00}, // ATmega16, ATmega164P, ATmega324P, ATmega324PA,
                        // ATmega644, ATmega644P, ATmega1284P, ATmega325,
                        // ATmega645, ATmega3250, ATmega6450,
    {0x75, 0x40, 0x00}, // ATmega329, ATmega329P, ATmega3290, ATmega3290P,
                        // ATmega649, ATmega649
    {0x76, 0x20, 0x00}, // ATmega8
    {0x78, 0x40, 0x00}, // ATmega169
    {0x7e, 0x40, 0x04}, // ATmega328
    {0x00, 0x00, 0x00}};

uint8_t SPI_speed = '1';

unsigned char gchar(void) {
  while (!Serial.available())
    ;
  return Serial.read();
}

void f910DoNothing(void) {}

void f910Unknown(void) { Serial.write('?'); }

void WriteProgramMemory(char cmd) {
  unsigned char data, a;

  data = gchar();
  if (data != 0xff) {
    if (cmd == 'C') {
      SPI.transfer(0x48);
    } else {
      SPI.transfer(0x40);
    }
    SPI.transfer(setting.addr >> 8);
    SPI.transfer(setting.addr & 0xff);
    SPI.transfer(data);
    if ((setting.polling == 0x00) || (data == setting.polling)) {
      delay(10);
    } else {
      do {
        if (cmd == 'C') {
          SPI.transfer(0x28);
        } else {
          SPI.transfer(0x20);
        }
        SPI.transfer(setting.addr >> 8);
        SPI.transfer(setting.addr & 0xff);
        a = SPI.transfer(0x00);
      } while (a != data);
    };
  };
  if (cmd == 'C')
    setting.addr++;
  Serial.write(0x0d);
}

void f910UniversalCommand(void) { //':' - Universal Command
  SPI.transfer(gchar());
  SPI.transfer(gchar());
  SPI.transfer(gchar());
  Serial.write(SPI.transfer(0x00));
  delay(50);
  Serial.write(0x0d);
}

void f910NewUniversalCommand(void) { //'.' - New universal Command
  SPI.transfer(gchar());
  SPI.transfer(gchar());
  SPI.transfer(gchar());
  Serial.write(SPI.transfer(gchar()));
  delay(50);
  Serial.write(0x0d);
}

void f910LoadAddress(void) { //'A' - Load Address
  setting.addr = gchar();
  setting.addr <<= 8;
  setting.addr |= gchar();
  Serial.write(0x0d);
}

void f910BlockWriteMemory(void) { //'B' Block Write Memory
  /**Pocet prijimanych bytu*/
  unsigned short count;
  /** E - eeprom, F - flash*/
  unsigned char MemoryType;
  unsigned short i, pageaddr;
  /** ma na bitech ktere odpovidaji adresovani uvnitr bloku zapisovaneho do cipu
   * 1 example: device.pagesize = 0x10 => MemoryPageMask = 0x0f */
  unsigned short MemoryPageMask;

  count = gchar(); // Velikos bloku dat
  count <<= 8;
  count |= gchar();
  if (count > SERIAL_RX_BUFFER_SIZE) {
    Serial.write('?');
    return;
  };
  MemoryType = gchar(); // Typ pameti
  switch (MemoryType) {
  case 'E':
    if (setting.eepromPageSize == 0x00) {
      Serial.write('?'); // Blokovy zapis neni podporovan
      break;
    }
    MemoryPageMask = setting.eepromPageSize - 1;
    pageaddr = setting.addr & (~MemoryPageMask);
    for (i = 0; i < count;) {
      SPI.transfer(0xC1);
      SPI.transfer(setting.addr >> 8);
      SPI.transfer(setting.addr & 0xff);
      SPI.transfer(gchar());

      i++;
      setting.addr++;

      if (((setting.addr & MemoryPageMask) == 0) || (i == count)) {
        SPI.transfer(0xC2);
        SPI.transfer(pageaddr >> 8);
        SPI.transfer(pageaddr & 0xff);
        SPI.transfer(0x00);
        pageaddr = setting.addr & (~MemoryPageMask);
        do {
          SPI.transfer(0xf0);
          SPI.transfer(0x00);
          SPI.transfer(0x00);
        } while ((SPI.transfer(0x00) & 0x01) != 0x00);
      };
    };
    Serial.write(0x0d);
    break;
  case 'F':
    if (setting.flashPageSize == 0x00) {
      Serial.write('?'); // Blokovy zapis neni podporovan
      return;
    }
    MemoryPageMask = setting.flashPageSize - 1;
    pageaddr = setting.addr & (~MemoryPageMask);
    for (i = 0; i < count;) {
      SPI.transfer(0x40);
      SPI.transfer(setting.addr >> 8);
      SPI.transfer(setting.addr & 0xff);
      SPI.transfer(gchar());

      i++;
      SPI.transfer(0x48);
      SPI.transfer(setting.addr >> 8);
      SPI.transfer(setting.addr & 0xff);
      SPI.transfer(gchar());
      i++;
      setting.addr++;
      if (((setting.addr & MemoryPageMask) == 0) || (i == count)) {
        SPI.transfer(0x4C);
        SPI.transfer(pageaddr >> 8);
        SPI.transfer(pageaddr & 0xff);
        SPI.transfer(0x00);
        pageaddr = setting.addr & (~MemoryPageMask);
        do {
          SPI.transfer(0xf0);
          SPI.transfer(0x00);
          SPI.transfer(0x00);
        } while ((SPI.transfer(0x00) & 0x01) != 0x00);
      };
    };
    Serial.write(0x0d);
    break;
  default:
    Serial.write('?');
    break;
  }
};

void f910WriteProgramMemoryHigh(void) { //'C' - Write Program Memory, High Byte
  WriteProgramMemory('C');
}

void f910WriteDataMemory(void) { //'D' - Write Data Memory
  SPI.transfer(0xC0);
  SPI.transfer(setting.addr >> 8);
  SPI.transfer(setting.addr & 0xff);
  SPI.transfer(gchar());
  Serial.write(0x0d);
}

void f910Exit(void) { //'E' - Exit , release all Ports, inhibit AVR910
  RESETN_CLR;
  SPI.end();
  LEDOFF;
  RESETN_DISABLE;
  Serial.write(0x0d);
}

void f910ReadFusebits(void) { //'F' - Read Fusebits (lfuse)
  SPI.transfer(0x50);
  SPI.transfer(0x00);
  SPI.transfer(0x00);
  Serial.write(SPI.transfer(0x00));
}

void f910LeaveProgMode(void) { //'L' - Leave Programming Mode
  f910Exit();
}

void f910ReadHightFusebits(void) { //'N' - Read high Fusebits
  SPI.transfer(0x58);
  SPI.transfer(0x08);
  SPI.transfer(0x00);
  Serial.write(SPI.transfer(0x00));
}

void f910EnterProgMode(void) { //'P' - Enter Programming Mode
  uint8_t i;
  uint8_t data;

  LEDON;
  for (i = 0; i < 32; i++) {
    RESETN_INIT;
    SPI_INIT;
    SPI.begin();
    switch (SPI_speed) {
    case '2':
      SPI.beginTransaction(SPISettings(345600, MSBFIRST, SPI_MODE0));
      break;
    case '1':
    default:
      SPI.beginTransaction(SPISettings(86400, MSBFIRST, SPI_MODE0));
    }
    delay(50);
    RESETN_CLR;
    delay(50);
    RESETN_SET;
    delay(50);
    SPI.transfer(0xAC);
    SPI.transfer(0x53);
    data = SPI.transfer(0x00);
    SPI.transfer(0x00);
    if (data == 0x53)
      break;
  }
  if (i < 32) {
    Serial.write(0x0d); // Proslo v poradku - inicializace hotova
  } else {
    LEDOFF;
    SPI.end();
    Serial.write('E'); // Nastala chyba
  }
}

void f910ReadExtendetFusebits(void) { //'Q' - Read extendet Fusebits
  SPI.transfer(0x50);
  SPI.transfer(0x08);
  SPI.transfer(0x00);
  Serial.write(SPI.transfer(0x00));
}

void f910ReadProgramMemory(void) { //'R' - Read Program Memory
  SPI.transfer(0x28);
  SPI.transfer(setting.addr >> 8);
  SPI.transfer(setting.addr & 0xff);
  Serial.write(SPI.transfer(0x00));
  SPI.transfer(0x20);
  SPI.transfer(setting.addr >> 8);
  SPI.transfer(setting.addr & 0xff);
  Serial.write(SPI.transfer(0x00));
}

void f910SwId(void) { //'S' - Return Software Identifier
  Serial.print(F(SW_ID));
}

void f910DeviceType(void) { //'T' - Device Type
  unsigned char i, b;

  setting.devcode = gchar();
  Serial.write(0x0d);
  setting.flashPageSize = 0;
  setting.eepromPageSize = 0;
  setting.addr = 0x00;
  for (i = 0; i < 255; i++) {
    b = pgm_read_byte(&Dev_M[i].devcode);
    if (b == 0x00)
      break;
    if (b == setting.devcode) {
      setting.flashPageSize = pgm_read_byte(&Dev_M[i].flashPageSize);
      setting.eepromPageSize = pgm_read_byte(&Dev_M[i].eepromPageSize);
    };
  };
  setting.polling = 0x00;
  for (i = 0; i < 255; i++) {
    b = pgm_read_byte(&Dev_S[i].devcode);
    if (b == 0x00)
      break;
    if (b == setting.devcode) {
      setting.polling = pgm_read_byte(&Dev_S[i].polling);
    };
  };
}

void f910SwVersion(void) { //'V' - Return Software Version
  Serial.print(F(SW_VER));
}

void f910AautoincrementAddressSupport(
    void) { //'a' - Return autoincrement address support
  Serial.print('Y');
}

void f910BlockWriteMode(void) { //'b' - Return Block write Mode support
  Serial.write('Y');
  Serial.write(SERIAL_RX_BUFFER_SIZE >> 8);
  Serial.write(SERIAL_RX_BUFFER_SIZE & 0xff);
}

void f910WriteProgramMemoryLow(void) { //'c' - Write Program Memory, Low Byte
  WriteProgramMemory('c');
}

void f910ReadDataMemory(void) { //'d' - Read Data Memory
  SPI.transfer(0xa8);
  SPI.transfer(setting.addr >> 8);
  SPI.transfer(setting.addr & 0xff);
  Serial.write(SPI.transfer(0x00));
  setting.addr++;
}

void f910ChipErase(void) { //'e' - Chip Erase
  SPI.transfer(0xac);
  SPI.transfer(0x80);
  SPI.transfer(0x00);
  SPI.transfer(0x00);
  Serial.write(0x0d);
}

void f910WriteFusebits(void) { //'f' - Write Fusebits
  SPI.transfer(0xac);
  SPI.transfer(0xa0);
  SPI.transfer(gchar());
  delay(10);
  Serial.write(0x0d);
}

void f910BlockReadMemory(void) { //'g' - Block Read Memory
  /** pocet zpracovavanyc bytu*/
  unsigned short i;
  /** E - eeprom, F - flash*/
  unsigned char MemoryType;
  i = gchar() << 8;
  i |= gchar();
  MemoryType = gchar();
  do {
    if (MemoryType == 'E') {
      SPI.transfer(0xa0);
    } else {
      SPI.transfer(0x20);
    }
    SPI.transfer(setting.addr >> 8);
    SPI.transfer(setting.addr & 0xff);
    Serial.write(SPI.transfer(0x00));
    if (MemoryType == 'F') {
      SPI.transfer(0x28);
      SPI.transfer(setting.addr >> 8);
      SPI.transfer(setting.addr & 0xff);
      Serial.write(SPI.transfer(0x00));
      i--;
    };
    i--;
    setting.addr++;
  } while (i > 0);
}

void f910ChipId(void) { //'i' - Return Chip ID (Note 14)
  Serial.print(F(CHIP_ID));
}

void f910WriteLockbits(void) { //'l' - Write Lockbits
  uint8_t a;

  a = gchar();
  SPI.transfer(0xac);
  SPI.transfer(a); // Pro Tiny a Mega
  SPI.transfer(0x00);
  SPI.transfer(a | 0xe0); // Pro 90s...
  delay(10);
  Serial.write(0x0d);
}

void f910WriteProgramMemoryPage(void) { //'m' - Write Program Memory Page
  SPI.transfer(0x4C);
  SPI.transfer(setting.addr >> 8);
  SPI.transfer(setting.addr & 0xff);
  SPI.transfer(0x00);
  delay(20);
  Serial.write(0x0d);
}

void f910ProgrammerType(void) { //'p' - Return Programmer Type
  Serial.write('S');
}

void f910ReadLockBits(void) { //'r' - Read Lock Bits
  SPI.transfer(0x58);
  SPI.transfer(0x00);
  SPI.transfer(0x00);
  Serial.write(SPI.transfer(0x00));
}

void f910ReadSignatureBytes(void) { //'s' - Read Signature Bytes
  unsigned char i;
  for (i = 2;; i--) {
    SPI.transfer(0x30);
    SPI.transfer(0x00);
    SPI.transfer(i);
    Serial.write(SPI.transfer(0x00));
    if (i == 0)
      break;
  }
}

void f910SupportedDevices(void) { //'t' - Show Supported Devices
  unsigned char i, d;
  for (i = 0;; i++) {
    d = pgm_read_byte(&Dev_S[i].devcode);
    if (d == 0x00)
      break;
    Serial.write(d);
  }
  for (i = 0;; i++) {
    d = pgm_read_byte(&Dev_M[i].devcode);
    if (d == 0x00)
      break;
    Serial.write(d);
  }
  Serial.write((char)0x00);
}

void f910HwVersion(void) { //'v' - Return Hardware Version
  Serial.print(F(HW_VER));
}

void f910SetLed(void) { //'x' - Set LED
  gchar();
  LEDON;
  Serial.write(0x0d);
}

void f910ClearLed(void) { //'y' - Clear LED
  gchar();
  LEDOFF;
  Serial.write(0x0d);
}

void f910SPISpeed(void) { //'z' - set SPI_speed
  SPI_speed = gchar();
  Serial.write(0x0d);
}

typedef void (*TAVR910function)(void);
const TAVR910function PROGMEM avr910functions[] = {
    (TAVR910function)f910Unknown,                      //  0, 0x 0
    (TAVR910function)f910Unknown,                      //  1, 0x 1
    (TAVR910function)f910Unknown,                      //  2, 0x 2
    (TAVR910function)f910Unknown,                      //  3, 0x 3
    (TAVR910function)f910Unknown,                      //  4, 0x 4
    (TAVR910function)f910Unknown,                      //  5, 0x 5
    (TAVR910function)f910Unknown,                      //  6, 0x 6
    (TAVR910function)f910Unknown,                      //  7, 0x 7
    (TAVR910function)f910Unknown,                      //  8, 0x 8
    (TAVR910function)f910Unknown,                      //  9, 0x 9
    (TAVR910function)f910Unknown,                      // 10, 0x a
    (TAVR910function)f910Unknown,                      // 11, 0x b
    (TAVR910function)f910Unknown,                      // 12, 0x c
    (TAVR910function)f910Unknown,                      // 13, 0x d
    (TAVR910function)f910Unknown,                      // 14, 0x e
    (TAVR910function)f910Unknown,                      // 15, 0x f
    (TAVR910function)f910Unknown,                      // 16, 0x10
    (TAVR910function)f910Unknown,                      // 17, 0x11
    (TAVR910function)f910Unknown,                      // 18, 0x12
    (TAVR910function)f910Unknown,                      // 19, 0x13
    (TAVR910function)f910Unknown,                      // 20, 0x14
    (TAVR910function)f910Unknown,                      // 21, 0x15
    (TAVR910function)f910Unknown,                      // 22, 0x16
    (TAVR910function)f910Unknown,                      // 23, 0x17
    (TAVR910function)f910Unknown,                      // 24, 0x18
    (TAVR910function)f910Unknown,                      // 25, 0x19
    (TAVR910function)f910Unknown,                      // 26, 0x1a
    (TAVR910function)f910DoNothing,                    // 27, 0x1b
    (TAVR910function)f910Unknown,                      // 28, 0x1c
    (TAVR910function)f910Unknown,                      // 29, 0x1d
    (TAVR910function)f910Unknown,                      // 30, 0x1e
    (TAVR910function)f910Unknown,                      // 31, 0x1f
    (TAVR910function)f910Unknown,                      // 32, 0x20,
    (TAVR910function)f910Unknown,                      // 33, 0x21, !
    (TAVR910function)f910Unknown,                      // 34, 0x22, "
    (TAVR910function)f910Unknown,                      // 35, 0x23, #
    (TAVR910function)f910Unknown,                      // 36, 0x24, $
    (TAVR910function)f910Unknown,                      // 37, 0x25, %
    (TAVR910function)f910Unknown,                      // 38, 0x26, &
    (TAVR910function)f910Unknown,                      // 39, 0x27, '
    (TAVR910function)f910Unknown,                      // 40, 0x28, (
    (TAVR910function)f910Unknown,                      // 41, 0x29, )
    (TAVR910function)f910Unknown,                      // 42, 0x2a, *
    (TAVR910function)f910Unknown,                      // 43, 0x2b, +
    (TAVR910function)f910Unknown,                      // 44, 0x2c, ,
    (TAVR910function)f910Unknown,                      // 45, 0x2d, -
    (TAVR910function)f910NewUniversalCommand,          // 46, 0x2e, .
    (TAVR910function)f910Unknown,                      // 47, 0x2f, /
    (TAVR910function)f910Unknown,                      // 48, 0x30, 0
    (TAVR910function)f910Unknown,                      // 49, 0x31, 1
    (TAVR910function)f910Unknown,                      // 50, 0x32, 2
    (TAVR910function)f910Unknown,                      // 51, 0x33, 3
    (TAVR910function)f910Unknown,                      // 52, 0x34, 4
    (TAVR910function)f910Unknown,                      // 53, 0x35, 5
    (TAVR910function)f910Unknown,                      // 54, 0x36, 6
    (TAVR910function)f910Unknown,                      // 55, 0x37, 7
    (TAVR910function)f910Unknown,                      // 56, 0x38, 8
    (TAVR910function)f910Unknown,                      // 57, 0x39, 9
    (TAVR910function)f910UniversalCommand,             // 58, 0x3a, :
    (TAVR910function)f910Unknown,                      // 59, 0x3b, ;
    (TAVR910function)f910Unknown,                      // 60, 0x3c, <
    (TAVR910function)f910Unknown,                      // 61, 0x3d, =
    (TAVR910function)f910Unknown,                      // 62, 0x3e, >
    (TAVR910function)f910Unknown,                      // 63, 0x3f, ?
    (TAVR910function)f910Unknown,                      // 64, 0x40, @
    (TAVR910function)f910LoadAddress,                  // 65, 0x41, A
    (TAVR910function)f910BlockWriteMemory,             // 66, 0x42, B
    (TAVR910function)f910WriteProgramMemoryHigh,       // 67, 0x43, C
    (TAVR910function)f910WriteDataMemory,              // 68, 0x44, D
    (TAVR910function)f910Exit,                         // 69, 0x45, E
    (TAVR910function)f910ReadFusebits,                 // 70, 0x46, F
    (TAVR910function)f910Unknown,                      // 71, 0x47, G
    (TAVR910function)f910Unknown,                      // 72, 0x48, H
    (TAVR910function)f910Unknown,                      // 73, 0x49, I
    (TAVR910function)f910Unknown,                      // 74, 0x4a, J
    (TAVR910function)f910Unknown,                      // 75, 0x4b, K
    (TAVR910function)f910LeaveProgMode,                // 76, 0x4c, L
    (TAVR910function)f910Unknown,                      // 77, 0x4d, M
    (TAVR910function)f910ReadHightFusebits,            // 78, 0x4e, N
    (TAVR910function)f910Unknown,                      // 79, 0x4f, O
    (TAVR910function)f910EnterProgMode,                // 80, 0x50, P
    (TAVR910function)f910ReadExtendetFusebits,         // 81, 0x51, Q
    (TAVR910function)f910ReadProgramMemory,            // 82, 0x52, R
    (TAVR910function)f910SwId,                         // 83, 0x53, S
    (TAVR910function)f910DeviceType,                   // 84, 0x54, T
    (TAVR910function)f910Unknown,                      // 85, 0x55, U
    (TAVR910function)f910SwVersion,                    // 86, 0x56, V
    (TAVR910function)f910Unknown,                      // 87, 0x57, W
    (TAVR910function)f910Unknown,                      // 88, 0x58, X
    (TAVR910function)f910Unknown,                      // 89, 0x59, Y
    (TAVR910function)f910Unknown,                      // 90, 0x5a, Z
    (TAVR910function)f910Unknown,                      // 91, 0x5b, [
    (TAVR910function)f910Unknown,                      // 92, 0x5c
    (TAVR910function)f910Unknown,                      // 93, 0x5d, ]
    (TAVR910function)f910Unknown,                      // 94, 0x5e, ^
    (TAVR910function)f910Unknown,                      // 95, 0x5f, _
    (TAVR910function)f910Unknown,                      // 96, 0x60, `
    (TAVR910function)f910AautoincrementAddressSupport, // 97, 0x61, a
    (TAVR910function)f910BlockWriteMode,               // 98, 0x62, b
    (TAVR910function)f910WriteProgramMemoryLow,        // 99, 0x63, c
    (TAVR910function)f910ReadDataMemory,               // 100, 0x64, d
    (TAVR910function)f910ChipErase,                    // 101, 0x65, e
    (TAVR910function)f910WriteFusebits,                // 102, 0x66, f
    (TAVR910function)f910BlockReadMemory,              // 103, 0x67, g
    (TAVR910function)f910Unknown,                      // 104, 0x68, h
    (TAVR910function)f910ChipId,                       // 105, 0x69, i
    (TAVR910function)f910Unknown,                      // 106, 0x6a, j
    (TAVR910function)f910Unknown,                      // 107, 0x6b, k
    (TAVR910function)f910WriteLockbits,                // 108, 0x6c, l
    (TAVR910function)f910WriteProgramMemoryPage,       // 109, 0x6d, m
    (TAVR910function)f910Unknown,                      // 110, 0x6e, n
    (TAVR910function)f910Unknown,                      // 111, 0x6f, o
    (TAVR910function)f910ProgrammerType,               // 112, 0x70, p
    (TAVR910function)f910Unknown,                      // 113, 0x71, q
    (TAVR910function)f910ReadLockBits,                 // 114, 0x72, r
    (TAVR910function)f910ReadSignatureBytes,           // 115, 0x73, s
    (TAVR910function)f910SupportedDevices,             // 116, 0x74, t
    (TAVR910function)f910Unknown,                      // 117, 0x75, u
    (TAVR910function)f910HwVersion,                    // 118, 0x76, v
    (TAVR910function)f910Unknown,                      // 119, 0x77, w
    (TAVR910function)f910SetLed,                       // 120, 0x78, x
    (TAVR910function)f910ClearLed,                     // 121, 0x79, y
    (TAVR910function)f910SPISpeed,                     // 122, 0x7a, z
    (TAVR910function)f910Unknown,                      // 123, 0x7b, {
    (TAVR910function)f910Unknown,                      // 124, 0x7c, |
    (TAVR910function)f910Unknown,                      // 125, 0x7d, }
    (TAVR910function)f910Unknown,                      // 126, 0x7e, ~
    (TAVR910function)f910Unknown,                      // 127, 0x7f
    (TAVR910function)f910Unknown,                      // 128, 0x80
    (TAVR910function)f910Unknown,                      // 129, 0x81
    (TAVR910function)f910Unknown,                      // 130, 0x82
    (TAVR910function)f910Unknown,                      // 131, 0x83
    (TAVR910function)f910Unknown,                      // 132, 0x84
    (TAVR910function)f910Unknown,                      // 133, 0x85
    (TAVR910function)f910Unknown,                      // 134, 0x86
    (TAVR910function)f910Unknown,                      // 135, 0x87
    (TAVR910function)f910Unknown,                      // 136, 0x88
    (TAVR910function)f910Unknown,                      // 137, 0x89
    (TAVR910function)f910Unknown,                      // 138, 0x8a
    (TAVR910function)f910Unknown,                      // 139, 0x8b
    (TAVR910function)f910Unknown,                      // 140, 0x8c
    (TAVR910function)f910Unknown,                      // 141, 0x8d
    (TAVR910function)f910Unknown,                      // 142, 0x8e
    (TAVR910function)f910Unknown,                      // 143, 0x8f
    (TAVR910function)f910Unknown,                      // 144, 0x90
    (TAVR910function)f910Unknown,                      // 145, 0x91
    (TAVR910function)f910Unknown,                      // 146, 0x92
    (TAVR910function)f910Unknown,                      // 147, 0x93
    (TAVR910function)f910Unknown,                      // 148, 0x94
    (TAVR910function)f910Unknown,                      // 149, 0x95
    (TAVR910function)f910Unknown,                      // 150, 0x96
    (TAVR910function)f910Unknown,                      // 151, 0x97
    (TAVR910function)f910Unknown,                      // 152, 0x98
    (TAVR910function)f910Unknown,                      // 153, 0x99
    (TAVR910function)f910Unknown,                      // 154, 0x9a
    (TAVR910function)f910Unknown,                      // 155, 0x9b
    (TAVR910function)f910Unknown,                      // 156, 0x9c
    (TAVR910function)f910Unknown,                      // 157, 0x9d
    (TAVR910function)f910Unknown,                      // 158, 0x9e
    (TAVR910function)f910Unknown,                      // 159, 0x9f
    (TAVR910function)f910Unknown,                      // 160, 0xa0
    (TAVR910function)f910Unknown,                      // 161, 0xa1
    (TAVR910function)f910Unknown,                      // 162, 0xa2
    (TAVR910function)f910Unknown,                      // 163, 0xa3
    (TAVR910function)f910Unknown,                      // 164, 0xa4
    (TAVR910function)f910Unknown,                      // 165, 0xa5
    (TAVR910function)f910Unknown,                      // 166, 0xa6
    (TAVR910function)f910Unknown,                      // 167, 0xa7
    (TAVR910function)f910Unknown,                      // 168, 0xa8
    (TAVR910function)f910Unknown,                      // 169, 0xa9
    (TAVR910function)f910Unknown,                      // 170, 0xaa
    (TAVR910function)f910Unknown,                      // 171, 0xab
    (TAVR910function)f910Unknown,                      // 172, 0xac
    (TAVR910function)f910Unknown,                      // 173, 0xad
    (TAVR910function)f910Unknown,                      // 174, 0xae
    (TAVR910function)f910Unknown,                      // 175, 0xaf
    (TAVR910function)f910Unknown,                      // 176, 0xb0
    (TAVR910function)f910Unknown,                      // 177, 0xb1
    (TAVR910function)f910Unknown,                      // 178, 0xb2
    (TAVR910function)f910Unknown,                      // 179, 0xb3
    (TAVR910function)f910Unknown,                      // 180, 0xb4
    (TAVR910function)f910Unknown,                      // 181, 0xb5
    (TAVR910function)f910Unknown,                      // 182, 0xb6
    (TAVR910function)f910Unknown,                      // 183, 0xb7
    (TAVR910function)f910Unknown,                      // 184, 0xb8
    (TAVR910function)f910Unknown,                      // 185, 0xb9
    (TAVR910function)f910Unknown,                      // 186, 0xba
    (TAVR910function)f910Unknown,                      // 187, 0xbb
    (TAVR910function)f910Unknown,                      // 188, 0xbc
    (TAVR910function)f910Unknown,                      // 189, 0xbd
    (TAVR910function)f910Unknown,                      // 190, 0xbe
    (TAVR910function)f910Unknown,                      // 191, 0xbf
    (TAVR910function)f910Unknown,                      // 192, 0xc0
    (TAVR910function)f910Unknown,                      // 193, 0xc1
    (TAVR910function)f910Unknown,                      // 194, 0xc2
    (TAVR910function)f910Unknown,                      // 195, 0xc3
    (TAVR910function)f910Unknown,                      // 196, 0xc4
    (TAVR910function)f910Unknown,                      // 197, 0xc5
    (TAVR910function)f910Unknown,                      // 198, 0xc6
    (TAVR910function)f910Unknown,                      // 199, 0xc7
    (TAVR910function)f910Unknown,                      // 200, 0xc8
    (TAVR910function)f910Unknown,                      // 201, 0xc9
    (TAVR910function)f910Unknown,                      // 202, 0xca
    (TAVR910function)f910Unknown,                      // 203, 0xcb
    (TAVR910function)f910Unknown,                      // 204, 0xcc
    (TAVR910function)f910Unknown,                      // 205, 0xcd
    (TAVR910function)f910Unknown,                      // 206, 0xce
    (TAVR910function)f910Unknown,                      // 207, 0xcf
    (TAVR910function)f910Unknown,                      // 208, 0xd0
    (TAVR910function)f910Unknown,                      // 209, 0xd1
    (TAVR910function)f910Unknown,                      // 210, 0xd2
    (TAVR910function)f910Unknown,                      // 211, 0xd3
    (TAVR910function)f910Unknown,                      // 212, 0xd4
    (TAVR910function)f910Unknown,                      // 213, 0xd5
    (TAVR910function)f910Unknown,                      // 214, 0xd6
    (TAVR910function)f910Unknown,                      // 215, 0xd7
    (TAVR910function)f910Unknown,                      // 216, 0xd8
    (TAVR910function)f910Unknown,                      // 217, 0xd9
    (TAVR910function)f910Unknown,                      // 218, 0xda
    (TAVR910function)f910Unknown,                      // 219, 0xdb
    (TAVR910function)f910Unknown,                      // 220, 0xdc
    (TAVR910function)f910Unknown,                      // 221, 0xdd
    (TAVR910function)f910Unknown,                      // 222, 0xde
    (TAVR910function)f910Unknown,                      // 223, 0xdf
    (TAVR910function)f910Unknown,                      // 224, 0xe0
    (TAVR910function)f910Unknown,                      // 225, 0xe1
    (TAVR910function)f910Unknown,                      // 226, 0xe2
    (TAVR910function)f910Unknown,                      // 227, 0xe3
    (TAVR910function)f910Unknown,                      // 228, 0xe4
    (TAVR910function)f910Unknown,                      // 229, 0xe5
    (TAVR910function)f910Unknown,                      // 230, 0xe6
    (TAVR910function)f910Unknown,                      // 231, 0xe7
    (TAVR910function)f910Unknown,                      // 232, 0xe8
    (TAVR910function)f910Unknown,                      // 233, 0xe9
    (TAVR910function)f910Unknown,                      // 234, 0xea
    (TAVR910function)f910Unknown,                      // 235, 0xeb
    (TAVR910function)f910Unknown,                      // 236, 0xec
    (TAVR910function)f910Unknown,                      // 237, 0xed
    (TAVR910function)f910Unknown,                      // 238, 0xee
    (TAVR910function)f910Unknown,                      // 239, 0xef
    (TAVR910function)f910Unknown,                      // 240, 0xf0
    (TAVR910function)f910Unknown,                      // 241, 0xf1
    (TAVR910function)f910Unknown,                      // 242, 0xf2
    (TAVR910function)f910Unknown,                      // 243, 0xf3
    (TAVR910function)f910Unknown,                      // 244, 0xf4
    (TAVR910function)f910Unknown,                      // 245, 0xf5
    (TAVR910function)f910Unknown,                      // 246, 0xf6
    (TAVR910function)f910Unknown,                      // 247, 0xf7
    (TAVR910function)f910Unknown,                      // 248, 0xf8
    (TAVR910function)f910Unknown,                      // 249, 0xf9
    (TAVR910function)f910Unknown,                      // 250, 0xfa
    (TAVR910function)f910Unknown,                      // 251, 0xfb
    (TAVR910function)f910Unknown,                      // 252, 0xfc
    (TAVR910function)f910Unknown,                      // 253, 0xfd
    (TAVR910function)f910Unknown,                      // 254, 0xfe
    (TAVR910function)f910Unknown                       // 255, 0xff
};

//***************************************************************************

void setup() {
  LEDINIT;
  ELEDINIT;
  HLEDINIT;
  BEEPINIT;

  LEDON;
  ELEDON;
  HLEDON;
  delay(200);
  LEDOFF;
  ELEDOFF;
  HLEDOFF;
  BEEPOFF;

  Serial.begin(115200);
}
//***************************************************************************

void loop() {
  unsigned char cmd;
  TAVR910function avr910Fun;

  cmd = gchar();
  memcpy_P(&avr910Fun, (void *)&(avr910functions[cmd]),
           sizeof(TAVR910function));
  avr910Fun();
}
